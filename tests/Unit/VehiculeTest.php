<?php

namespace Tests\Unit;

use App\FluxFinance;
use App\RapportFinance;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use phpDocumentor\Reflection\Types\Void_;
use Tests\TestCase;

class VehiculeTest extends TestCase
{
    use RefreshDatabase;

    protected $vehicule;

    protected function setUp(): void
    {
        parent::setUp();

        $this->vehicule = create("App\Vehicule");

    }

    /** @test */
    function un_vehicule_peut_avoir_des_options()
    {

        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $this->vehicule->options
        );
    }

    /** @test */
    function un_vehicule_a_un_type_de_vehicule()
    {
        $this->assertInstanceOf('App\TypeVehicule', $this->vehicule->typeVehicule);
    }


    /** @test */
    function un_vehicule_a_une_carte_grise()
    {
        $this->withoutExceptionHandling();
        $carte = create('App\CarteGrise',['vehicule_id'=>$this->vehicule->id]);
        $this->assertInstanceOf('App\CarteGrise', $this->vehicule->carte_grise);
    }

    /** @test */
    function un_vehicule_au_moins_une_assurance()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->vehicule->assurances);
    }

    /** @test */
    function un_vehicule_a_des_visites_techniques()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->vehicule->visitesTechniques);
    }



    // /** @test */
    // public function a_vehicule_can_add_a_reply()
    // {
    //     $this->vehicule->addReply([
    //         'body' => 'Foobar',
    //         'user_id' => 1
    //     ]);

    //     $this->assertCount(1, $this->vehicule->replies);
    // }
}
