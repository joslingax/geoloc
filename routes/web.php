<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['register' => false]);
Route::get('flux-finances/export/', 'API\FluxFinanceController@export');
Route::get('locations/export/', 'API\LocationController@export');
Route::get('voitures/export/', 'API\VehiculeController@export');
Route::get('depenses/export/', 'API\DepenseController@export');
Route::get('clients/export/', 'API\ClientController@export');
Route::get('utilisateurs/export/', 'API\UserController@export');
Route::middleware('auth')->get('historiques/export', 'API\HistoriqueController@export');

Route::middleware('auth')->get('clients/{id}/download', 'API\ClientController@getFile');

Route::middleware('auth')->get('chauffeurs/{id}/download', 'API\ChauffeurController@getFile');
Route::middleware('auth')->get('chauffeurs/export', 'API\ChauffeurController@export');



Route::get('/{any}', 'HomeController@index')->where('any', '.*');