<?php

namespace App\Http\Controllers\API;

use App\Marque;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MarqueController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per =  request()->query("per_page")  ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        if($per)
        return  Marque::with(['pays','modeles'])->search($q)->orderBy("marques.libelle",'asc')->paginate($per);
        else
        return  Marque::with(['pays','modeles'])->search($q)->orderBy("marques.libelle",'asc')->get();

           
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'libelle' => 'required|unique:roles|max:255',
            'pays_id' => 'required|exists:pays,id',
        ]);

        Marque::create([
            'libelle' => $request->input('libelle'),
            'pays_id' => $request->input('pays_id'),
        ]);

        return response()->json([
            'message' => 'Marque ajoutée avec succès'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function show(Marque $marque)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function edit(Marque $marque)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marque $marque)
    {

        
        $validatedData = $request->validate([
            'libelle' => 'required|max:255|unique:roles,libelle,'.$marque->id,
            'pays_id' => 'required|exists:pays,id'
        ]);
       
        $marque->libelle = $request->input('libelle');
        $marque->pays_id = $request->input('pays_id');

        $marque->save();

        return response()->json([
            'message' => 'Marque modifié avec succès',
            'entity' => $marque],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marque $marque)
    {

        //on supprime
        $marque->delete();
        return response()->json([
            'message' => 'Marque supprimé avec succès',
            'entity' => $marque],200);

    }
}
