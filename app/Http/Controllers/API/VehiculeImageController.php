<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Vehicule;
use Illuminate\Http\Request;
use DB;
use Log;

class VehiculeImageController extends Controller
{
    public function updateImages(Request $request,$id)
    {
        try
        {

   
            
            //on commence la transaction
            DB::beginTransaction();
            $message = "Aucune Modification transmise";
            $profileWasUpdated = false;
            $vehicule =Vehicule::whereId($id)->first();

         
            //Image de mise en avant
            if(($request->input('image_mise_en_avant')=="" || $request->input('image_mise_en_avant')==null ) && $vehicule->hasPhoto )
            {
               //On retire l'image de mise zn avant
                if(file_exists(storage_path('app/public/images/'.$vehicule->image_mise_en_avant)))
                {
                    unlink(storage_path('app/public/images/'.$vehicule->image_mise_en_avant));
                }
                $vehicule->image_mise_en_avant = null;
                $vehicule->save();
                $profileWasUpdated = true;
               
            }
            elseif(($request->input('image_mise_en_avant')!="" && $request->input('image_mise_en_avant')!=null ) && $vehicule->hasPhoto == false )
            {
               //On ajoute une nouvelle image
               $file = $request->input('image_mise_en_avant');
                //On retire l'image de mise zn avant
               $vehicule->image_mise_en_avant = $vehicule->getFileName($vehicule->getMime($file));
               $vehicule->save();
               $vehicule->uploadImage($file,$vehicule->getMime($file),$vehicule->fresh()->image_mise_en_avant);
               $profileWasUpdated = true;


               
            }
            elseif(($request->input('image_mise_en_avant')!="" && $request->input('image_mise_en_avant')!=null ) && $vehicule->hasPhoto && $request->input('updated') )
            {
               //On ajoute une nouvelle image
               $file = $request->input('image_mise_en_avant');
               if(file_exists(storage_path('app/public/images/'.$vehicule->image_mise_en_avant)))
               {
                   unlink(storage_path('app/public/images/'.$vehicule->image_mise_en_avant));
               }
               $vehicule->image_mise_en_avant = $vehicule->getFileName($vehicule->getMime($file));
               $vehicule->save();
               $vehicule->uploadImage($file,$vehicule->getMime($file),$vehicule->fresh()->image_mise_en_avant);
               $profileWasUpdated = true;
               
            }
      
            //Image de mise en avant

            
            DB::commit();
            
            return response()->json(['success' =>true,'updated'=>$profileWasUpdated,'entity'=>$vehicule ? $vehicule->fresh(): null],201);

     }
     catch(\Exception $e)
     {
             DB::rollback();
             Log::debug("something bad happened");
             Log::debug($e->getMessage());
             return response()->json(['success' => false,"message"=>$e->getMessage()],201);
     }
         
    }
}
