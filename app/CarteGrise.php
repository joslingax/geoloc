<?php

namespace App;

use App\Traits\CanUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class CarteGrise extends SModel
{
    use CanUpload, LogsActivity;

    protected static $logAttributes = ["date_etablissement","profession","proprietaire","adresse","date_premiere_mise_en_circulation","fichier","nr_serie_type","nr_immatriculation","nr_precedent_immatriculation","vehicule_neuf","poids_vide","poids_total_en_charge","charge_utile","nbr_place","puissance_administrative","carosserie","genre","vehicule_id","marque_id","modele_id","type_motorisation_id"];
    protected static $logName = 'carte grise';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;

    //
    protected $table="carte_grises";
    protected $storage_path ="public/documents/cartes_grises";

    
    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé la carte grise <strong>{$this->nr_immatriculation}</strong> de la voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé la carte grise n° <strong>{$this->nr_immatriculation}</strong> de la voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié la carte grise n° <strong>{$this->nr_immatriculation}</strong> de la voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié la carte grise n°  <strong>{$this->nr_immatriculation}</strong> de la voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté la carte grise n° <strong>{$this->nr_immatriculation}</strong> à la voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté la carte grise n° <strong>{$this->nr_immatriculation}</strong> à la voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        
    }

    public function vehicule()
    {
        return $this->belongsTo("App\Vehicule");
    }

    public function immatriculeVehicule(Vehicule $vehicule)
    {
        $this->vehicule()->save($vehicule);
    }
}
