<?php

namespace App\Exports;

use App\Vehicule;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class VehiculeExport implements FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize,WithTitle
{

    use Exportable; 

    private $search;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($search)
    {
        $this->search =$search;
    }
    
    public function title(): string
    {
        return 'voitures';
    }
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Vehicule::query()->search($this->search);
    }
 
   //use Exportable; 

    public function map($vehicule): array
    {
        return [
            $vehicule->libelle,
            $vehicule->marque->libelle,
            $vehicule->modele->libelle,
            $vehicule->couleur,
            $vehicule->plaque_immatriculation,
            $vehicule->boite_vitesse,
            $vehicule->typeVehicule->libelle,
            $vehicule->typeMotorisation->libelle

        ];
    }

    public function headings(): array
    {
        return [
            "Nom ",
            "marque",
            'modele',
            'couleur',
            'immatriculation',
            "boite de vitesse",
            'type véhicule',
            "énergie"
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
