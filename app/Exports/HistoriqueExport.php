<?php

namespace App\Exports;

use App\Historique;
use Spatie\Activitylog\Models\Activity;
use App\Vehicule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class HistoriqueExport implements FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize,WithTitle
{

    use Exportable; 

    private $search,$dictionnary,$q,$dbt, $fin,$user ;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($q,$dbt, $fin,$user)
    {
        $this->search = $q;
        $this->dbt = $dbt;
        $this->fin = $fin;
        $this->user = $user;
        $this->dictionnary = Config::get('tables');
    }
    public function title(): string
    {
        return 'Historiques';
    }
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Historique::query()->causedBy($this->user)->periode($this->dbt,$this->fin);
    }


 
    //use Exportable; 

    public function map($historique): array
    {
        return [
            $this->getDescription($historique),
            $this->getType($historique),
            $historique->created_at->format('m/d/Y à H:m'),

        ];
    }

    public function headings(): array
    {
        return [
            "Description",
            "Type",
            'Enrégistré le',
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }

    public function getDescription($activity): String
    {
        $description = "";
       // $activity->load(['causeur','subject']);
        $crud = collect(['deleted','created','updated']);
        $auth= collect(['connexion','deconnexion']);

        $sujet ="";
        //Sujet
        if($activity->causer==null)
        {
            $sujet .= " un utilisateur";
        }
        else
        {
            $sujet .= " l'utilisateur ".$activity->causer->name;
        }

        //GRoupe verbal
        $gv = "";
        $complement = "";
        if($crud->contains($activity->description))
        {
            //Groupe verbal
            if($activity->description=='created')
            {
                $gv .= " a ajouté ";
            }
            else if($activity->description=='updated')
            {
                $gv .= " a modifié ";
            }
            else
            {
                $gv .= " a supprimé  ";

            }

            //Le Complèment de la phrase

            
                foreach($this->dictionnary as $entry)
                {
                    if($entry['table']==$activity->log_name)
                    {
                        $complement .= $entry['defined'];  
                        break;
                    }
                }


        }
        else if($auth->contains($activity->description))
        {
            if($activity->description=='connexion')
            {
                $gv .= " s'est connecté à l'application ";
            }
            else
            {
                $gv .= " s'est déconnecté de l'application ";
            }
        }
        else
        {
            $gv .= " a effectué une action non répertoriée ";
        }

        $description = $sujet." ".$gv." ".$complement;

        return $description;
    }

    public function getType($activity): String
    {

        $crud = collect(['deleted','created','updated']);
        $auth= collect(['connexion','deconnexion']);
        if($crud->contains($activity->description))
        {
            //Groupe verbal
            if($activity->description=='created')
            {
                return "création";
            }
            else if($activity->description=='updated')
            {
                return "modification";
            }
            else
            {
                return "supprésion";

            }
        }
        else if($auth->contains($activity->description))
        {
            Log::info("AUTH");
            if($activity->description=='connexion')
            {
                return "connexion";
            }
            else
            {
                return "déconnexion";
            }
        }
        else
        {
            return "non répertorié";
        }
    }
}