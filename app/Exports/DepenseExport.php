<?php

namespace App\Exports;

use App\Depense;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class DepenseExport implements WithTitle, FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{

    use Exportable; 
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Depense::query();
    }

    public function title(): string
    {
        return 'depenses';
    }
 
    //use Exportable; 

    public function map($depense): array
    {
        return [
            $depense->date_facturation,
            $depense->description,
            $depense->montant,
            $depense->vehicule->libelle,
            $depense->type_depense->libelle,


        ];
    }

    public function headings(): array
    {
        return [
            "Date de facturation ",
            "Description",
            'Montant',
            'Voiture Concerné',
            'Type de dépense'
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
