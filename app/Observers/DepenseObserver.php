<?php

namespace App\Observers;

use App\Depense;
use App\FluxFinance;
use Carbon\Carbon;

class DepenseObserver
{
    /**
     * Handle the depense "created" event.
     *
     * @param  \App\Depense  $depense
     * @return void
     */
    public function created(Depense $depense)
    {
        //
        FluxFinance::create(
            [
              "flux"=> $depense->description." [".Carbon::parse($depense->date_facturation)->format('d-m-Y')."]",
              "montant"=> $depense->montant,
              "financiable_id"=> $depense->id,
              "date_transaction"=>Carbon::parse($depense->date_facturation)->format('Y-m-d'),
              "vehicule_id"=> $depense->vehicule_id,
              "financiable_type"=>"App\Depense",

            ]);
    }

    /**
     * Handle the depense "updated" event.
     *
     * @param  \App\Depense  $depense
     * @return void
     */
    public function updated(Depense $depense)
    {
        $flux = FluxFinance::where('financiable_id',$depense->id)->where('financiable_type',"App\Depense")->first();
        $flux->montant = $depense->montant;
        $flux->date_transaction = $depense->date_facturation;
        $flux->save();
    }

    /**
     * Handle the depense "deleted" event.
     *
     * @param  \App\Depense  $depense
     * @return void
     */
    public function deleted(Depense $depense)
    {
        //

        $flux = FluxFinance::where('financiable_id',$depense->id)->where('financiable_type','\App\Depense')->first();
        if($flux)
        {
            $flux->delete();
        }

    }

    /**
     * Handle the depense "restored" event.
     *
     * @param  \App\Depense  $depense
     * @return void
     */
    public function restored(Depense $depense)
    {
        //
    }

    /**
     * Handle the depense "force deleted" event.
     *
     * @param  \App\Depense  $depense
     * @return void
     */
    public function forceDeleted(Depense $depense)
    {
        //
    }
}
