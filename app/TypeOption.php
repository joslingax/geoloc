<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOption extends SModel
{
    public function options()
    {
        return $this->hasMany("App\Option");

    }
}
