<?php

namespace App\ViewRecords;

use Illuminate\Database\Eloquent\Model;

class VehicleRecord extends Model
{

    protected $table ="vehicule_vue";
    protected $primaryKey = 'id';

    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('booking_id', 'LIKE', "%{$q}%")
                ->orWhere('trip_date', 'LIKE', "%{$q}%")
                ->orWhere('created_at', 'LIKE', "%{$q}%")
                ->orWhere('buyer_name', 'LIKE', "%{$q}%")
                ->orWhere('route_name', 'LIKE', "%{$q}%")
                ->orWhere('departure', 'LIKE', "%{$q}%")
                ->orWhere('seats', 'LIKE', "%{$q}%")
                ->orWhere('booking_id', 'LIKE', "%{$q}%")
                ->orWhere('payment', 'LIKE', "%{$q}%")
                ->orWhere('amount', 'LIKE', "%{$q}%");
    }
}
