<?php

namespace App;

use App\Traits\CanUpload;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Spatie\Activitylog\Traits\LogsActivity;

class Vehicule extends SModel implements Searchable
{
    use LogsActivity;

    protected static $logAttributes = ["libelle","marque_id","modele_id","type_vehicule_id","type_permis_id","nbr_place","nbr_porte","couleur","tarif","kilometrage","plaque_immatriculation","nr_chassis","date_mise_en_circulation","annee_mise_en_circulation","consommation_au_100_km","type_motorisation_id","boite_vitesse","volume_coffre","volume_reservoir","largeur","longueur","poids","hauteur"];
    protected static $logName = 'vehicule';
    protected static $logOnlyDirty = true; 
    protected static $submitEmptyLogs = false;
    public static function getFinanceOverTime($dbt, $fin)
    {
        $dbt = Carbon::parse($dbt);
        $fin = Carbon::parse($fin);
        //On recupere toutes les locations durant cette periode de temps
        $locations = \App\Location::all();

        //dd($locations);

    }
   use CanUpload;

    protected $storage_path ="public/images";

    protected $appends = ['path','title',"hasPhoto"];

    public function getHasPhotoAttribute()
    {
        if($this->image_mise_en_avant == null) return false;
        return file_exists(storage_path("/app/public/images/".$this->image_mise_en_avant)) ? true : false;
    }


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé la  voiture <strong>{$this->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé la  voiture <strong>{$this->libelle}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié  la  voiture <strong>{$this->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié la  voiture <strong>{$this->libelle}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté  la voiture <strong>{$this->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté  la voiture <strong>{$this->libelle}</strong>";
        }
        
    }
    
    public function getSearchResult(): SearchResult{
        $search = new SearchResult(
            $this,
            $this->libelle,
            $this->chemin()
        );
    $search->setType("Voiture");
    return $search;
    }

    //Chemin d'acces à un véhicule
    public function chemin()
    {
       return "/vehicules/".$this->id;
    }

    //Chemin d'acces à un véhicule
    public function getPathAttribute()
    {
        if($this->image_mise_en_avant)
        return Storage::url('images/'.$this->image_mise_en_avant);
        else
        return null;
    }

    public function getTitleAttribute()
    {
        return $this->libelle." |  ".ucfirst($this->marque()->first()->libelle)." ".ucfirst($this->modele()->first()->libelle);
    }
    public function ajouteOption($option)
   {
       if(is_integer($option))
       {
           $this->options()->attach($option);
       }
       else
       {
           $this->options()->save($option);
       }
   }

    public function ajouteCarteGrise($carte)
    {
        if(is_array($carte))
        {
            $this->cartesGrises()->create($carte);
        }
        else
        {
            $this->cartesGrises()->save($carte);
        }
    }

    public function ajouteLocation($location)
    {
        if(is_array($location))
        {
            $this->locations()->create($location);
        }
        else
        {
            $this->locations()->save($location);
        }
    }


    public function ajouteVisiteTechnique($visite)
    {
        if(is_array($visite))
        {
            $this->visitesTechniques()->create($visite);
        }
        else
        {
            $this->visitesTechniques()->save($visite);
        }
    }
    public function ajouteAssurance($assurance)
    {
        if(is_array($assurance))
        {
            $this->assurances()->create($assurance);
        }
        else
        {
            $this->assurances()->save($assurance);
        }
    }

    //RELATIONS DBT //////////////////////////////////////////////////////////////

    public function marque()
    {
        return $this->belongsTo("App\Marque");

    }

    public function modele()
    {
        return $this->belongsTo("App\Modele");


    }

    public function typeVehicule()
    {
        return $this->belongsTo("App\TypeVehicule");


    }

    public function typeMotorisation()
    {
        return $this->belongsTo("App\TypeMotorisation");


    }
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
        ->orWhere('vehicules.libelle', 'LIKE', "%{$q}%")
        ->orWhere('vehicules.couleur', 'LIKE', "%{$q}%")
        ->orWhere('vehicules.boite_vitesse', 'LIKE', "%{$q}%")
        ->orWhere('vehicules.date_mise_en_circulation', 'LIKE', "%{$q}%")
        ->orWhere('vehicules.plaque_immatriculation', 'LIKE', "%{$q}%")
        ->orWhere('marques.libelle', 'LIKE', "%{$q}%")
        ->orWhere('modeles.libelle', 'LIKE', "%{$q}%")
        ->orWhere('type_motorisations.libelle', 'LIKE', "%{$q}%")
        ->orWhere('type_vehicules.libelle', 'LIKE', "%{$q}%")
        ->leftJoin('marques', 'marques.id', '=', 'vehicules.marque_id')
        ->leftJoin('type_vehicules', 'type_vehicules.id', '=', 'vehicules.type_vehicule_id')
        ->leftJoin('type_motorisations', 'type_motorisations.id', '=', 'vehicules.type_motorisation_id')
        ->leftJoin('modeles', 'modeles.id', '=', 'vehicules.modele_id');
    }

    public function options()
    {
        return $this->belongsToMany("App\Option",'option_vehicule','vehicule_id','option_id');
    }

    public function images()
    {
        return $this->hasMany("App\Image");

    }

    public function carte_grise()
    {
        return $this->hasOne("App\CarteGrise");

    }

    public function locations()
    {
        return $this->hasMany("App\Location")->orderBy('locations.date_dbt','desc');

    }
    public function assurances()
    {
        return $this->hasMany("App\Assurance");

    }

    public function assurance()
    {
        $now = Carbon::now();
        return $this->hasMany("App\Assurance")->where('date_dbt','<=',$now->toDateString())
        ->where('date_fin','>=',$now->toDateString());

    }

    public function visitesTechniques()
    {
        return $this->hasMany("App\VisiteTechnique",'vehicule_id');

    }


    public function visite_technique()
    {
        $now = Carbon::now();
        return $this->hasMany("App\VisiteTechnique")->where('date_dbt','<=',$now->toDateString())
        ->where('date_fin','>=',$now->toDateString());

    }
    //RELATIONS END///////////////////////////////////////////////////////////////////////////////

    public function getMarqueAttribute()
    {
        return $this->marque()->first();

    }
}
