<?php

namespace App\Listeners;

use App\Events\LocationHasBeenAdded;
use App\Events\LocationHasBeenPaid;
use App\FluxFinance;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddPaidFluxFinanceItem
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LocationHasBeenAdded  $event
     * @return void
     */
    public function handle(LocationHasBeenPaid $event)
    {
                //On recalcule date durée du  de la location
                $nbr_jours = Carbon::parse($event->location->date_dbt)->diffInDays(Carbon::parse($event->location->date_fin));
                // on ajoute un numéro unique a la location
                $event->location->numero = "LC".Carbon::now()->year."-".$event->location->id;
                

                $event->location->frais_livraison     = $event->location->livraison_effective ? 5000 : 0;
                $frais = $event->location->livraison_effective ? 5000 : 0  + $event->location->lavage_carburant;  
        
                $event->location->montant   = $nbr_jours*$event->location->tarif + $frais;
                $event->location->nbr_jour  = $nbr_jours;

                $event->location->save();
        
                //on sauvegarde
                $event->location->save();
                //puis on rafraichit le modele
                $event->location->load('statutPayement');
        
        
                $jours = ($event->location->nbr_jour > 1) ? "jours":"jour";
                $prefix1 = "Paiement total";
                $prefix2 = $event->location->parent_id!=null ? " : Prolongement n°".$event->location->numero : ": Location n° ".$event->location->numero ;
                $description =$prefix1." ".$prefix2." pour ".$event->location->nbr_jour." ".$jours." : Du "
                              .Carbon::parse($event->location->date_dbt)->format('d-m-Y')." au "
                              .Carbon::parse($event->location->date_fin)->format('d-m-Y');
        
                
                   //on génére un flux financier
                  
                  if($event->location->statutPayement->nature=="payé")
                  FluxFinance::create(
                    [
                      "flux"=> $description,
                      "montant"=> $event->location->montant,
                      "financiable_id"=> $event->location->id,
                      "vehicule_id"=> $event->location->vehicule_id,
                      "date_transaction"=>Carbon::parse($event->location->date_dbt)->format('Y-m-d'),
                      "financiable_type"=>"App\Location",
        
                    ]);
    }
}
