<?php

namespace App\Listeners;

use App\Events\LocationHasBeenRemoved;
use App\FluxFinance;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RemoveFluxFinanceItem
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LocationHasBeenRemoved  $event
     * @return void
     */
    public function handle(LocationHasBeenRemoved $event)
    {
        $flux = FluxFinance::where('financiable_id',$event->location->id)->where('financiable_type','App\Location')->get();
        
        if($flux->isNotEmpty())
        {
            $flux->each(function ($item, $key) {
                
                $item->delete();

            });
        }
    }
}
