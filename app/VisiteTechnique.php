<?php

namespace App;

use App\Traits\CanUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class VisiteTechnique extends SModel
{
    use CanUpload,LogsActivity;

    protected static $logAttributes = ["date_fin","date_dbt","effectue_par",'vehicule_id','nr'];
    protected static $logName = 'visite technique';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé la visite technique <strong>{$this->nr}</strong> effectuée  <strong>{ucFirst($this->effectue_par)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé la visite technique n° <strong>{$this->nr}</strong> effectuée  <strong>{ucFirst($this->effectue_par)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié la visite technique n° <strong>{$this->nr}</strong> effectuée  <strong>{ucFirst($this->effectue_par)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié la visite technique n°  <strong>{$this->nr}</strong> effectuée  <strong>{ucFirst($this->effectue_par)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté la visite technique n° <strong>{$this->nr}</strong> effectuée  <strong>{ucFirst($this->effectue_par)}</strong> à la voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté la visite technique n° <strong>{$this->nr}</strong> à la voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        
    }
    public function vehicule()
    {
        return $this->belongsTo("App\Vehicule");
    }
}
