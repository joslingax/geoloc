<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class Pays extends SModel
{
    use LogsActivity;

    protected static $logAttributes = ["libelle"];
    protected static $logName = 'pays';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;
}
