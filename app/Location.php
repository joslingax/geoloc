<?php

namespace App;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Location extends SModel implements Searchable
{
    use LogsActivity;

    protected static $logAttributes = ["client_id","numero","vehicule_id","chauffeur_id","parent_id","avec_chauffeur","client_est_chauffeur","livraison_effective","longue_duree","niveau_carburant","statut","date_dbtdate_finnbr_journom_chauffeur","nom_chauffeur_2","montant","tarif","lavage_carburant","frais_livraison","observation","zone_location"];
    protected static $logName = 'location';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;
    
    protected $appends = ["periode","en_cours","durationOnSameMonth","running",'over','waiting','description'];
    protected $dates = ['created_at', 'updated_at'];


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        $numero = "LC".Carbon::now()->year."-".$this->id;
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé la location {$numero}";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé la location {$numero}";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié la location {$numero}";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié la location {$numero}";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté la location {$numero}";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté la location {$numero}";
        }
        
    }
        /**
     * Get a string path for the thread.
     *
     * @return string
     */

    public function getSearchResult(): SearchResult{

        $search = new SearchResult(
                $this,
                $this->description,
                $this->chemin()
            );
        $search->setType("Location");
        return $search;
    }


    public function chemin()
    {
        return '/locations/'. $this->id;
    }

    //Verifie si la location se termine en début de mois
    public function endinOnStartOfMonth()
    {
        return (Carbon::parse($this->date_fin)->day=="01" || Carbon::parse($this->date_fin)->day=="01" ) ? true : false;
    }



    public function getDurationOnSameMonthAttribute()
    {
        $dbt = Carbon::parse($this->date_dbt);
        $fin = Carbon::parse($this->date_fin);

         return ($dbt->month==$fin->month) ? true : false;
    }


    public function getPeriodeAttribute()
{
    $dbt = Carbon::parse($this->date_dbt);
    $fin = Carbon::parse($this->date_fin);

    return $dbt->format('d/m/Y')." - ".$fin->format('d/m/Y');
}
   //argent généré par une location chaque jour durant toute la
   //durée de la location
    public function detailsFinanciersJournaliers()
    {
        $periodeLocation = Carbon::parse($this->date_dbt)->daysUntil($this->date_fin);
        $periodes = $periodeLocation->toArray();
        // Iterate over the period
        $untils = [];
        for ($i=0;$i<count($periodes);++$i)

            if($i!=(count($periodes)))
            {
                $periode = $periodes[$i];
                $flux = [
                    "date" =>$periodes[$i]->format('Y-m-d'),
                    "date_dbt"=>$periodes[$i]->format('Y-m-d'),
                    "date_fin"=>$periodes[$i]->addDay()->format('Y-m-d'),

                    "montant"=>$this->tarif + ($this->getTaxes()/$this->nbr_jour)
                ];

                array_push($untils,$flux);
            }


        // Convert the period to an array of dates
          return collect($untils);
    }

    public function getSalesOverTime($dbt,$fin)
    {
        $amount = 0 ;
        $dbt = Carbon::parse($dbt);
        $fin = Carbon::parse($fin);
        $salesPerDay = $this->detailsFinanciersJournaliers();

        foreach ($salesPerDay as $sale)
        {
            if(Carbon::parse($sale['date'])->between($dbt,$fin))
            {
                $amount += $sale['montant'];
            }
        }
        return $amount;
    }

    public function getEnCoursAttribute()
    {
       $now =Carbon::now();
       $dbt = Carbon::parse($this->date_dbt);
       $fin = Carbon::parse($this->date_fin);
       if($dbt->greaterThanOrEqualTo($now))
       return "En attente";
       else if($now->between($dbt,$fin))
       return "En cours";
       else
       return "Terminé";
    }

    public function getRunningAttribute()
    {
       $now =Carbon::now();
       $dbt = Carbon::parse($this->date_dbt);
       $fin = Carbon::parse($this->date_fin);
       if($now->between($dbt,$fin))
       return true;
       else
       return false;
    }
    public function getOverAttribute()
    {
       $now =Carbon::now();
       $fin = Carbon::parse($this->date_fin);
       if($now->isAfter($fin))
       return true;
       else
       return false;
    }
    public function getWaitingAttribute()
    {
       $now =Carbon::now();
       $dbt = Carbon::parse($this->date_dbt);
       if($now->isBefore($dbt))
       return true;
       else
       return false;
    }
    public function getTaxes()
    {
        return $this->frais_livraison + $this->lavage_carburant;
    }





    public function prolongements()
    {
        return $this->hasMany("App\Prolongement");


    }

    public function ajouteProlongement()
    {
        return $this->belongsTo("App\Vehicule");


    }
    /**
     * Ajoute un prolongement à la locations.
     *
     * @param $prolongement
     */
    public function addReply($prolongement)
    {
        $this->prolongements()->create($prolongement);
    }
     ////////////////////////////////////////////////////////
    //  ATTRIBUTES DBT
    ///////////////////////////////////////////////////////
    public function getDescriptionAttribute()
    {
        $jours = ($this->nbr_jour > 1) ? "jours":"jour";
        //$prefix1 =  ($this->statutPayement->nature=="payé") ? "Paiement total" : (($this->statutPayement->nature=="avance") ? "Avance sur paiement" : "" );
        $prefix2 = $this->parent_id!=null ? "Prolongement N°".$this->numero : "Location n° ".$this->numero ;
        return $description = $prefix2." pour ".$this->nbr_jour." ".$jours." : Du "
                      .Carbon::parse($this->date_dbt)->format('d-m-Y')." au "
                      .Carbon::parse($this->date_fin)->format('d-m-Y');
    }
     ////////////////////////////////////////////////////////
    //  ATTRIBUTES FIN
    ///////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////
    //SCOPE FILTER DBT
    ///////////////////////////////////////////////////////
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
        ->orWhere('locations.date_dbt', 'LIKE', "%{$q}%")
        ->orWhere('locations.date_fin', 'LIKE', "%{$q}%")
        ->orWhere('locations.nom_chauffeur', 'LIKE', "%{$q}%")
        ->orWhere('locations.nom_chauffeur_2', 'LIKE', "%{$q}%")
        ->orWhere('locations.nbr_jour', 'LIKE', "%{$q}%")
        ->orWhere('locations.created_at', 'LIKE', "%{$q}%")
        ->orWhere('vehicules.libelle', 'LIKE', "%{$q}%")
        ->orWhere('clients.nom', 'LIKE', "%{$q}%")
        ->orWhere('clients.prenom', 'LIKE', "%{$q}%")
        ->leftJoin('clients', 'clients.id', '=', 'locations.client_id')
        ->leftJoin('vehicules', 'vehicules.id', '=', 'locations.vehicule_id');
    }

    public function scopeStatut($query, $q)
    {
        if ( $q =="avance" || $q =="payé" || $q =="reservé") return $query
        ->where('statut_payements.nature', 'LIKE', "%{$q}%")
        ->leftJoin('statut_payements', 'statut_payements.id', '=', 'locations.statut_payement_id');
        else
        return $query;

    }

    public function scopeDeroulement($query, $q)
    {
        if ( $q =="running" || $q =="over" || $q =="waiting") 
        {
            $now = Carbon::now();
            if($q =="running")
            {

                return $query->whereDate('date_dbt','<=', $now->toDateTimeString())
                             ->whereDate('date_fin','>=', $now->toDateTimeString());
            }
            elseif($q=="over")
            {
                return $query ->whereDate('date_fin','<', $now->toDateTimeString());
            }
            else
            {
                return $query ->whereDate('date_dbt','>', $now->toDateTimeString());

            }
        }
        else
        return $query;

    }

    public function scopePeriodSearch($query, $date_dbt,$date_fin)
    {
        if ($date_fin== null || $date_dbt == null) return $query;
        else
        {
        return $query->where(function($query)  use ($date_dbt,$date_fin)
            {
                $query->where('date_dbt', '>=', $date_dbt)
                      ->where('date_dbt', '<=', $date_fin);
            })->orWhere(function($query)  use ($date_dbt,$date_fin)
            {
                $query->where('date_fin', '>=', $date_dbt)
                      ->where('date_fin', '<=', $date_fin);
            });
        }

    }
    public function scopeFilterByVehicle($query, $vehicule_id)
    {
        if ($vehicule_id == null ) return $query;
        else
        {
            return $query->where('vehicule_id',$vehicule_id);
        }

    }

    ////////////////////////////////////////////////////////
    //SCOPE FILTER FIN
    ///////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////
    //RELATIONS DBT
    ///////////////////////////////////////////////////////
    public function client()
    {
        return $this->belongsTo("App\Client");

    }

     /**
     * Une location est enregistrée par un utilisateur.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createur()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

         /**
     * Une location est enregistrée par un utilisateur.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function chauffeur()
    {
        return $this->belongsTo(Chauffeur::class, 'chauffeur_id');
    }
        
         /**
     * Une location est enregistrée par un utilisateur.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statutPayement()
    {
        return $this->belongsTo(StatutPayement::class, 'statut_payement_id');
    }

    public function fluxfinances()
    {
        return $this->morphMany('App\FluxFinance', 'financiable','financiable_type');
    }
    public function vehicule()
    {
        return $this->belongsTo("App\Vehicule");

    }

     ////////////////////////////////////////////////////////
    //RELATIONS FIN
    ///////////////////////////////////////////////////////



}
