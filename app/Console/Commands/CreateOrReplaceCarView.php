<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
class CreateOrReplaceCarView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:CreateOrReplaceCarView';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a view for cars in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement("
        CREATE OR REPLACE VIEW vehicule_vue
        AS
        SELECT
           v.*,
           m.libelle  as marque,
           tv.libelle as type_vehicule,
           tp.libelle as type_permis,
           tp.libelle as type_motorisation
        FROM
           vehicules v
            LEFT JOIN  marques m ON v.marque_id = m.id
            LEFT JOIN type_vehicules tv ON tv.id = v.type_vehicule_id
            LEFT JOIN type_motorisations tm ON tm.id = v.type_motorisation_id
            LEFT JOIN type_permis tp ON tp.id = v.type_permis_id

    ");
    }
}
