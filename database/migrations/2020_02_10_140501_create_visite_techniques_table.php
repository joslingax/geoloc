<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisiteTechniquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visite_techniques', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string("nr");
            $table->string("effectue_par");
            $table->date("date_dbt");
            $table->string("date_fin");
            $table->string("fichier",1000)->nullable();
            $table->unsignedBigInteger('vehicule_id');

            $table->timestamps();

            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visite_techniques');
    }
}
