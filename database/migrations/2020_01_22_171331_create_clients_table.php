<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('nom',50)->nullable();
            $table->string('prenom',50)->nullable();
            $table->date('date_naissance')->nullable();
            $table->index(['nom','prenom']);
            //contacts
            $table->string('telephone1',30)->nullable();
            $table->string('telephone2',30)->nullable();
            $table->string('email')->unique()->nullable();


            //Adresse
            $table->string('adresse',100)->nullable();
            $table->string('bp',100)->nullable();
            $table->string('quartier',100)->nullable();
            $table->string('ville',100)->nullable();
            
            $table->string('nr_permis_conduire')->nullable();
            $table->unsignedBigInteger('pays_id')->nullable();

            $table->timestamps();

            $table->foreign('nr_permis_conduire')->references('numero')->on('permis')->onDelete('set null');
            $table->foreign('pays_id')->references('id')->on('pays')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
