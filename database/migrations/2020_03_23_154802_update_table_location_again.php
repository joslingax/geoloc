<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableLocationAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) 
        {
            $table->string('numero')->unique()->nullable()->after('id');
            $table->unsignedBigInteger('parent_id')->nullable()->after('vehicule_id');
           // $table->foreign('parent_id')->references('id')->on('locations')->onDelete('set null');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
  
        });
    }
}
