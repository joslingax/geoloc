<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depenses', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string("description");
            $table->double("montant");
            $table->date('date_facturation');

            $table->unsignedBigInteger('vehicule_id')->nullable();
            $table->unsignedBigInteger('type_depense_id')->nullable();
            
            $table->timestamps();

            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('set null');
            $table->foreign('type_depense_id')->references('id')->on('type_depenses')->onDelete('set null');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depenses');
    }
}
