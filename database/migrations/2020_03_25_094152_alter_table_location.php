<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('locations', function (Blueprint $table) {
            $table->unsignedBigInteger('chauffeur_id')->nullable()->after('vehicule_id');
            $table->foreign('chauffeur_id')->references('id')->on('chauffeurs')->onDelete('set null');
        });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('chauffeur_id');
        });
    }
}
