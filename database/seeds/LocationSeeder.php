<?php

use App\Vehicule;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory("App\StatutPayement")->create(['nature'=>"payé"]);
         factory("App\StatutPayement")->create(['nature'=>"avance"]);
         factory("App\StatutPayement")->create(['nature'=>"reservé"]);
    }
}
