<?php

use App\Permission;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = ["vehicule","assurance","carte_grise","client","depense","flux_finance","location","marque","modele","option","pays","permis","permission","role","type_motorisation",'utilisateur'];

        $permissions = [
               [
                   "fr"=>"ajoute",
                   "en"=>"create"
               ],
              [
                "fr"=>"supprime",
                "en"=>"delete"
            ],
            [
                "fr"=>"modifie",
                "en"=>"edit"
            ],
            [
                "fr"=>"accede",
                "en"=>"access"
            ],
            [
                "fr"=>"exporte",
                "en"=>"export"
            ]
        ];

    foreach($tables as $table)
    {
        foreach($permissions as $permission)
        {
            factory("App\Permission")->create([
                'nom' =>$permission['fr']."-".$table,
                'table' =>$table
                    ]);
                    
        }
    }

     factory("App\Role")->create(['nom'=>"super-admin","libelle"=>"super administrateur"]);
     factory("App\Role")->create(['nom'=>"admin","libelle"=>"administrateur"]);
     factory("App\Role")->create(['nom'=>"gerant","libelle"=>"gérant"]);

     $jos=factory('App\User')->create(['email'=>"joslingax@geoloc.com","name"=>"joslin Issiga","username"=>"joslingax"]);
     $nene=factory('App\User')->create(['email'=>"souke@geoloc.com","name"=>"M. Souke","username"=>"souke.souke"]);
     $nene=factory('App\User')->create(['email'=>"nene.rostin@geoloc.com","name"=>"rostin Nene","username"=>"rostin.nene"]);
     $tch=factory('App\User')->create(['email'=>"epse.tchibinda@geoloc.com","name"=>"Mme Tchibinda","username"=>"mme.tchibinda"]);
     $mouss=factory('App\User')->create(['email'=>"romanic.moussavou@geoloc.com","name"=>"romanic Moussavou","username"=>"joslingax"]);
     $tch=factory('App\User')->create(['email'=>"yahn.tchibinda@geoloc.com","name"=>"Yahn Tchibinda"]);

     factory('App\User',40)->create();

     $jos->assignRole('super-admin');
     $nene->assignRole('super-admin');
     $tch->assignRole('super-admin');
     $mouss->assignRole('super-admin');
}
}
